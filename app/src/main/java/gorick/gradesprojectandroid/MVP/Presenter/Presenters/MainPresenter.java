package gorick.gradesprojectandroid.MVP.Presenter.Presenters;

import java.math.BigDecimal;

/**
 * Created by sg-0036936 on 07/03/2017.
 */

public class MainPresenter {

    private BigDecimal[] listGrades;
    private Integer[] listFaults;
    private String[] listClasses;


    public void setListGrades(BigDecimal[] listGrades, Integer[] listFaults, String[] listClasses) {

        this.listClasses = listClasses;
        this.listFaults = listFaults;
        this.listGrades = listGrades;

    }

    public Integer[] getListFaults() {
        return listFaults;
    }

    public BigDecimal[] getListGrades() {
        return listGrades;
    }

    public String[] getListClasses() {
        return listClasses;
    }
}
